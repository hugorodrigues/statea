// SPDX-FileCopyrightText: 2016, 2022 Hugo Rodrigues
//
// SPDX-License-Identifier: MIT

function parseParameters(parameters) {
    var params = {}

    for (var index = 0; index < parameters.length; index++) {
        var param_splited = parameters[index].split('=');
        var variable = param_splited[0];
        var value = param_splited[1];
        params[variable] = decodeURIComponent(value);
    }

    return params;
}


function getUrlParameters() {
    return parseParameters(location.search.substr(1).split('&'));
}

function getHashParameters() {
    return parseParameters(location.hash.substr(1).split("&"));
}
