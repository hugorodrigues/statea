// Configuration file

"use strict";

// Set the base URL for you Gitea instance (don't forget the https:// if using HTTPS)
const BASE_URL = "https://corsproxy.io/?https://codeberg.org";

// No need to change this since it's based on BASE_URL
const API_URL = `${BASE_URL}/api/v1`;

// Set CORS mode
// Valid values are cors, no-cors, or same-origin
const CORS_MODE = "cors";

// Repo owner and name
const REPO_OWNER = "hugorodrigues";
const REPO_NAME = "statea-test";

// This label sets the issues used for service status
const STATUS_LABEL = "status";

// This label sets the issues used for scheduled maintenance
const MAINTENANCE_LABEL = "maintenance";

// How many issues do we load each time
const API_PAGE_SIZE = 50;

// Format to display time and date
// Check https://github.com/thdoan/strftime#supported-conversion-specifications
const TIME_FORMAT = "%c";

// Debug mode?
const DEBUG = true;

// Name of gitea instance
const GITEA_INSTANCE_NAME = "Codeberg";
