// SPDX-FileCopyrightText: 2022 Hugo Rodrigues
//
// SPDX-License-Identifier: MIT

"use strict";

const generateUUID = () => {
  if (typeof crypto === 'object') {
    if (typeof crypto.randomUUID === 'function') {
      // https://developer.mozilla.org/en-US/docs/Web/API/Crypto/randomUUID
      return crypto.randomUUID();
    }
    if (typeof crypto.getRandomValues === 'function' && typeof Uint8Array === 'function') {
      // https://stackoverflow.com/questions/105034/how-to-create-a-guid-uuid
      const callback = (c) => {
        const num = Number(c);
        return (num ^ (crypto.getRandomValues(new Uint8Array(1))[0] & (15 >> (num / 4)))).toString(16);
      };
      return '10000000-1000-4000-8000-100000000000'.replace(/[018]/g, callback);
    }
  }
  let timestamp = new Date().getTime();
  let perforNow = (typeof performance !== 'undefined' && performance.now && performance.now() * 1000) || 0;
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) => {
    let random = Math.random() * 16;
    if (timestamp > 0) {
      random = (timestamp + random) % 16 | 0;
      timestamp = Math.floor(timestamp / 16);
    } else {
      random = (perforNow + random) % 16 | 0;
      perforNow = Math.floor(perforNow / 16);
    }
    return (c === 'x' ? random : (random & 0x3) | 0x8).toString(16);
  });
};

function addLoadingScreen(message, debug_message) {
    const messages = document.getElementById("loadingmsg");
    let element = document.createElement("div");
    if (message != undefined) {
        element.innerText = message;
    }
    if (DEBUG && debug_message != undefined) {
        let debugEle = document.createElement("div");
        debugEle.innerHTML = `&#x1FAB2; ${debug_message}`;
        element.appendChild(debugEle);
    }
    messages.appendChild(element);
    document.getElementById("loading").classList.remove("hidden");
    return element;
};

function delLoadingScreen(element) {
    element.remove();
    if (document.getElementById("loadingmsg").children.length == 0) {
        document.getElementById("loading").classList.add("hidden");
    }
}
