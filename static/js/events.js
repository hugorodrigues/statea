// SPDX-FileCopyrightText: 2022 Hugo Rodrigues
//
// SPDX-License-Identifier: MIT

"use strict";

const enableOverlayEvent = new Event("enableOverlayEvent", {
    bubbles: false,
    cancelable: false
});

const disableOverlayEvent = new Event("disableOverlayEvent", {
    bubbles: false,
    cancelable: false
});

// Will be used to store overlays default DOM
var overlayDefaults = {};

function eventHashChange(event) {
    let params = getHashParameters();

    let overlay = "index";
    if ("overlay" in params) {
        overlay = params.overlay;
    }
    console.debug("Using overlay " + overlay);

    let overlay_doms = document.querySelectorAll("[overlay-name=" + overlay + "]")
    if (overlay_doms.length == 0) {
        console.error("Unknown overlay " + overlay);
        location.hash="#overlay=error&message=Invalid overlay";
        return;
    }

    for ( let i = 0; i < overlay_doms.length; i++) {
        overlay_doms[i].dispatchEvent(enableOverlayEvent);
    }
}

function eventEnableOverlay(event) {

    const load = addLoadingScreen(undefined, "Enable overlay");

    // Disable previous overlays
    let active = document.getElementsByClassName("overlay-active");
    for ( let j = 0; j < active.length; j++) {
        active[j].dispatchEvent(disableOverlayEvent);
    }

    // Check if this overlay has a uuid
    // If not, we will generate one

    let overlayUUID = event.target.getAttribute("overlay-uuid");
    if ( overlayUUID == undefined) {
        overlayUUID = generateUUID();
        event.target.setAttribute("overlay-uuid", overlayUUID);
    }

    // Make a clone of DOM if needed
    if ( !(overlayUUID in overlayDefaults) ) {
        overlayDefaults[overlayUUID] = event.target.cloneNode(true);
    }

    // Call enable function
    let overlayPtr = window[event.target.getAttribute("overlay-enable-fn")]
    if ( overlayPtr == undefined) {
        console.debug("No overlay enable function for " + event.target.getAttribute("overlay-name"))
    } else {
        overlayPtr(event);
    }

    // Mark it as active
    event.target.classList.add("overlay-active");

    delLoadingScreen(load);
}

function eventDisableOverlay(event) {

    const load = addLoadingScreen(undefined, "Disable overlay");

    // Call disable function
    let overlayPtr = window[event.target.getAttribute("overlay-disable-fn")]
    if ( overlayPtr == undefined) {
        console.debug("No overlay disable function for " + event.target.getAttribute("overlay-name"))
    } else {
        overlayPtr(event);
    }

    // Restore original DOM
    let overlayUUID = event.target.getAttribute("overlay-uuid");
    if ( overlayUUID != undefined && overlayUUID in overlayDefaults ) {
        const newTarget = overlayDefaults[overlayUUID].cloneNode(true);
        addOverlayEvents(newTarget);
        event.target.replaceWith(newTarget);
    } else {
        console.exception("No DOM clone for this overlay");
        event.target.classList.remove("overlay-active");
    }

    delLoadingScreen(load);
}

// Don't add this function to a event listener
// Each event should have his own function
// that should call this
function eventInfiniteScroll(event, root, callback) {
    if (window.innerHeight + Math.ceil(window.pageYOffset) >= document.body.offsetHeight - 2) {
        if (root.getAttribute("statea-loading") == "1") {
            console.debug("loading data, ignoring scroll");
        } else if (root.getAttribute("gitea-has-data") == "0") {
            console.debug("No more data on gitea, ignoring scroll");
        } else  {
            root.setAttribute("statea-loading", "1");
            callback(event, root);
        }
    }
}

// Use this function to populate the initial data for the infinite scroll
function initInfiniteScroll(event, root, callback) {

    const obsCallback = function(mutationList, observer) {
        mutationList.forEach(function(mutation) {
            if (mutation.type === "attributes") {
                if (mutation.attributeName == "statea-loading" && mutation.target.getAttribute("statea-loading") == "0") {
                    if (window.innerHeight > document.body.offsetHeight) {
                        callback(event);
                    } else {
                        observer.disconnect();
                    }
                } else if (mutation.target.getAttribute("gitea-has-data") == "0") {
                    observer.disconnect();
                }
            }
        });
    }

    const obsHistory = new MutationObserver(obsCallback);
    obsHistory.observe(root, {attributes: true});

    // First call
    window.addEventListener("scroll", callback);
    callback(event);
}

function addOverlayEvents(target) {
    target.addEventListener("enableOverlayEvent", eventEnableOverlay);
    target.addEventListener("disableOverlayEvent", eventDisableOverlay);
}

function setupEvents() {
    let overlays = document.getElementsByClassName("overlay");
    for ( let i = 0; i < overlays.length; i++) {
        addOverlayEvents(overlays[i]);
    }

    window.addEventListener("hashchange", eventHashChange);
}
