"use strict";

function eventOverlayLicensesEnable(event) {
    let headers = document.getElementsByTagName("header");
    for( let i=0; i < headers.length; i++) {
        headers[i].classList.add("hidden");
    }
    document.querySelector("[href='#overlay=licenses']").parentElement.classList.add("pure-menu-selected");
    requestGetText("/static/assets/licenses.txt", function (data, extra) {

        let code = document.createElement("pre");
        code.innerText = data;

        event.target.appendChild(code);
    });
}

function eventOverlayLicensesDisable(event) {
    let headers = document.getElementsByTagName("header");
    for( let i=0; i < headers.length; i++) {
        headers[i].classList.remove("hidden");
    }
    document.querySelector("[href='#overlay=licenses']").parentElement.classList.remove("pure-menu-selected");
}
