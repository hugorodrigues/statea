function eventOverlayIssueScroll(event, first_run) {
    const history = document.getElementById("issue.comments");
    let block = false;
    if (first_run == true) {
        block = true;
    }
    eventInfiniteScroll(event, history, function() {

        const param = getHashParameters();

        const issue_id = param["issue"];
        const page = parseInt(history.getAttribute("gitea-page"));
        callGitea(`/repos/${REPO_OWNER}/${REPO_NAME}/issues/${issue_id}/timeline?&page=${page}&limit=${API_PAGE_SIZE}`, callbackComment, {"event": event, "page": page, "root": history}, undefined, block);
    });
}

function eventOverlayIssueEnable(event) {

    let param = getHashParameters();

    let issue_id = param["issue"];

    if (issue_id == undefined) {
        location.hash="#overlay=error&message=No issue provided";
        return;
    }

    callGitea(`/repos/${REPO_OWNER}/${REPO_NAME}/issues/${issue_id}`, function(issue) {
        const [is_valid, is_maintenance] = parseIssueStatus(issue);
        const now = new Date();
        const [date_open, date_close] = getIssueOpenCloseDate(issue);
        const [maintenance_start, maintenance_end] = getMaintenanceDates(issue);
        let icon = "";
        let tag_class = "";
        let tag_text = "";
        if (is_maintenance) {
            document.getElementById("issue.comments").setAttribute("statea-issue-type", "maintenance");
            icon = "&#x1F6A7;";
            if (issue["state"] == "open") {
                if (maintenance_start > now) {
                    tag_class = "up";
                    tag_text = "Scheduled";
                } else if (maintenance_end > now) {
                    tag_class = "up";
                    tag_text = "Ongoing";
                } else {
                    tag_class = "down";
                    tag_text = "Ongoing";
                }
            } else {
                tag_class = "up";
                tag_text = "Completed";
            }
        } else {
            document.getElementById("issue.comments").setAttribute("statea-issue-type", "issue");
            icon = "&#x1F6D1;";
            if (issue["state"] == "open") {
                tag_text = "Ongoing";
                tag_class = "down";
            } else {
                tag_text = "Fixed";
                tag_class = "up";
            }
        }

        // Only init infinite scroll after storing the issue type on DOM
        initInfiniteScroll(event, document.getElementById("issue.comments"), eventOverlayIssueScroll);

        // Title
        let title = document.getElementById("issue.title");
        title.innerHTML = `${icon} ${issue["title"]}`;

        let tag = document.createElement("span");
        tag.classList.add("tag");
        tag.classList.add("right");
        tag.classList.add(tag_class);
        tag.innerText = tag_text;
        title.appendChild(tag);

        // Start and end dates
        let dates = document.getElementById("issue.dates");

        let start_dt = document.createElement("dt");
        start_dt.innerText = "Opened at";

        let start_dd = document.createElement("dd");

        if (is_maintenance) {
            start_dd.innerText = strftime(TIME_FORMAT, maintenance_start);
        } else {
            start_dd.innerText = strftime(TIME_FORMAT, date_open);
        }
        dates.appendChild(start_dt);
        dates.appendChild(start_dd);


        if (date_close || maintenance_end != undefined && maintenance_end.getFullYear() < 2100) {

            let end_dt = document.createElement("dt");
            end_dt.innerText = "Closed at";

            let end_dd = document.createElement("dd");

            if (is_maintenance) {
                end_dd.innerText = strftime(TIME_FORMAT, maintenance_end);
            } else {
                end_dd.innerText = strftime(TIME_FORMAT, date_close);
            }
            dates.appendChild(end_dt);
            dates.appendChild(end_dd);
        }

        // Extra info
        let einfo = document.getElementById("issue.extra_info");
        let openu_dt = document.createElement("dt");
        openu_dt.innerText = `Created by`;

        let openu_dd = document.createElement("dd");
        openu_dd.innerText = `${issue["user"]["full_name"]}`;

        einfo.appendChild(openu_dt);
        einfo.appendChild(openu_dd);

        if (issue["assignee"] != undefined) {

            let assignees_dt = document.createElement("dt");
            assignees_dt.innerText = `Assigned to`;

            let assignees = "";

            issue["assignees"].forEach(function (user) {

                if (issue["assignee"]["id"] == user["id"] && issue["assignees"].length > 1) {
                    assignees = `,<strong>${user["full_name"]}</strong>`;
                } else {
                    assignees = `,${user["full_name"]}`;
                }
            });

            let assignees_dd = document.createElement("dd");
            assignees_dd.innerHTML = assignees.substring(1);

            einfo.appendChild(assignees_dt);
            einfo.appendChild(assignees_dd);
        }

        parseMarkdown("comment", issue["body"], false, function(issue_html) {
            let body = document.getElementById("issue.body");
            body.innerHTML = issue_html;
            body.classList.remove("hidden");
        });

    });

}

function eventOverlayIssueDisable(event) {
    window.removeEventListener("scroll", eventOverlayIssueScroll);
}
