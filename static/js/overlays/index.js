// SPDX-FileCopyrightText: 2022 Hugo Rodrigues
//
// SPDX-License-Identifier: MIT

"use strict";

// ==== index ====

function callbackActiveIssues(data, extra) {
    const active_incidents = document.getElementById("index.incidents");

    // Do status
    data.forEach(function(issue) {
        createStateArticle(active_incidents, issue, ["down", "down-active"], `Opened at ${strftime(TIME_FORMAT, new Date(issue["created_at"]))} with ${issue["comments"]} comments`);
        issue.labels.forEach(function(label) {
            if (label.name in extra["services"]) {
                extra["services"][label.name].classList.remove("up");
                extra["services"][label.name].classList.add("down");
            }
        });
    });
    if (data.length == 0) {
        document.getElementById("index.weareok").classList.remove("hidden");
    } else {
        active_incidents.classList.remove("hidden");
    }
}

function callbackActiveMaintenance(data, extra) {
    const active_maintenance = document.getElementById("index.maintenance");
    const future_maintenance = document.getElementById("index.scheduled");

    const now = new Date();

    let show_active = false;
    let show_future = false;

    data.forEach(function(issue){
        const [start, end] = getMaintenanceDates(issue);
        const start_str = strftime(TIME_FORMAT, start);
        const end_str = strftime(TIME_FORMAT, end);

        if (start > now) {
            createStateArticle(future_maintenance, issue, ["up"], `Scheduled to start at ${start_str}`);
            show_future = true;
        } else if (end > now) {
            createStateArticle(active_maintenance, issue, ["up"], `Scheduled to end at ${end_str}`);
            show_active = true;
        } else {
            createStateArticle(active_maintenance, issue, ["down", "down-active"], `Was supposed to end at ${end_str}`);
            show_active = true;
        }

    });

    if (show_active) {
        active_maintenance.classList.remove("hidden");
    }
    if (show_future) {
        future_maintenance.classList.remove("hidden");
    }
}

function callbackLive(data, extra) {
    const live = document.getElementById("index.live");
    let services = {};

    data.forEach(function(service) {

        let article = document.createElement("article");
        article.classList.add("up");
        article.classList.add("pure-g");
        article.classList.add("link");

        let article_info = document.createElement("div");
        article_info.classList.add("pure-u-1-1");
        let article_title = document.createElement("h4");
        article_title.innerText = service["name"];
        let article_sum = document.createElement("div");
        article_sum.innerText = service["description"];
        article_info.appendChild(article_title);
        article_info.appendChild(article_sum);
        article.appendChild(article_info);

        article.addEventListener("click", function(event) {
            location.hash = `#overlay=service&service=${service["name"]}`;
        });

        services[service["label"]] = article;

        live.appendChild(article);
    });

    if (data.length > 0) {
        live.classList.remove("hidden");
    }

    // Get active issues
    // We do it in instead of on overlay enable since it requires the services to be loaded
    callGitea(`/repos/${REPO_OWNER}/${REPO_NAME}/issues?state=open&type=issues&labels=${STATUS_LABEL}`, callbackActiveIssues, {"event": event, "services": services});
}

function eventOverlayIndexScroll(event, first_run) {
    let block = false;
    if (first_run == true) {
        block = true;
    }
    const history = document.getElementById("index.history");
    eventInfiniteScroll(event, history, function() {
        const page = parseInt(history.getAttribute("gitea-page"));
        // Get closed issues and maintenace
        // NB: Apparently Gitea API can't search issues with only of the labels
        //     so we need to filter the labels our selfs
        callGitea(`/repos/${REPO_OWNER}/${REPO_NAME}/issues?state=closed&type=issues&page=${page}&limit=${API_PAGE_SIZE}`, callbackHistory, {"event": event, "page": page, "root": history}, undefined, block);
    });
}

function eventOverlayIndexEnable(event) {

    // Get live status + active issues (check end of callbackLive)
    requestGetJson("/static/assets/services.json", callbackLive, {"event": event});

    // Get active maintenances
    callGitea(`/repos/${REPO_OWNER}/${REPO_NAME}/issues?state=open&type=issues&labels=${MAINTENANCE_LABEL}`, callbackActiveMaintenance, {"event": event});

    // Get repository information
    callGitea(`/repos/${REPO_OWNER}/${REPO_NAME}`, function(data) {
        document.getElementsByTagName("header")[0].innerText = data["description"];
    });

    // Get history
    initInfiniteScroll(event, document.getElementById("index.history"), eventOverlayIndexScroll);
}

function eventOverlayIndexDisable(event) {
    window.removeEventListener("scroll", eventOverlayIndexScroll);
}

// ==== end index ====
