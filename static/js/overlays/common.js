"use strict";

const RE_MAINTENANCE_START = RegExp('^start: (.+)$', "m");
const RE_MAINTENANCE_END = RegExp('^end: (.+)$', "m");

function parseIssueStatus(issue) {
    const valid_labels = [STATUS_LABEL, MAINTENANCE_LABEL];
    let valid = false;
    let is_maintenance = false;
    issue["labels"].forEach(function(label) {
        if (valid_labels.includes(label.name)) {
            valid = true;
            is_maintenance = label.name == MAINTENANCE_LABEL
            return;
        }
    });

    return [valid, is_maintenance];
}

function getMaintenanceDates(issue) {
    let start = RE_MAINTENANCE_START.exec(issue.body);
    let end = RE_MAINTENANCE_END.exec(issue.body);

    if (start == null) {
        console.exception(`No start date was specified for #${issue.number}`);
        start = new Date("1970-01-01");
    } else {
        start = undefined;
    }

    if (end == null) {
        console.exception(`No end date was specified for #${issue.number}`);
        end = new Date("2100-01-01");
    } else {
        end = undefined;
    }

    return [start, end];
}

function getIssueOpenCloseDate(issue) {
    let open = new Date(issue["created_at"]);
    let close = undefined;

    if (issue["closed_at"]) {
        close = new Date(issue["closed_at"]);
    }

    return [open, close];
}

function createStateArticle(parent, issue, classes, summary) {
    let article = document.createElement("article");
    classes.forEach(function(cls) {
        article.classList.add(cls);
    });

    article.classList.add("pure-g");

    let article_info = document.createElement("div");
    article_info.classList.add("pure-u-2-3");
    let article_title = document.createElement("h4");
    article_title.innerText = issue["title"];
    let article_sum = document.createElement("div");
    article_sum.innerText = summary;
    article_info.appendChild(article_title);
    article_info.appendChild(article_sum);
    article.appendChild(article_info);

    let article_linkd = document.createElement("div");
    article_linkd.classList.add("pure-u-1-3");
    article_linkd.classList.add("text-right");
    let article_link = document.createElement("a");
    article_link.innerText = `Incident #${issue["number"]} report →`;
    article_link.href=`#overlay=issue&issue=${issue["number"]}`;
    article_linkd.appendChild(article_link);
    article.appendChild(article_linkd);


    parent.appendChild(article);
}

function callbackHistory(data, extra) {
    const history = extra["root"];
    if (data.length == 0) {
        history.setAttribute("gitea-has-data", "0");
        history.setAttribute("statea-loading", "0");
        return;
    }
    history.setAttribute("gitea-has-data", "1");
    let show = false;
    data.forEach(function(issue) {

        let [valid, is_maintenance] = parseIssueStatus(issue);

        if (!valid) {
            return;
        }

        let summary;
        let classes;
        if (is_maintenance) {
            summary = "Completed";
            classes = ["up"];
        } else {
            summary = "";
            classes = ["down"];
        }

        createStateArticle(history, issue, classes, summary);
        show = true;

    });
    if (show) {
        history.classList.remove("hidden");
    }
    history.setAttribute("gitea-page", extra["page"] + 1);
    history.setAttribute("statea-loading", "0");
}

function callbackComment(comments, extra) {
    if (comments == undefined || comments.length == 0) {
        extra["root"].setAttribute("gitea-has-data", "0");
        extra["root"].setAttribute("statea-loading", "0");
        return;
    }
    const issue_type = extra["root"].getAttribute("statea-issue-type");
    comments.forEach(function (comment) {

        if ( ! ["comment", "close", "reopen"].includes(comment["type"])) {
            return;
        }

        let article = document.createElement("article");
        article.classList.add("hidden");

        let header = document.createElement("div");
        header.classList.add("pure-g");

        let op = document.createElement("div");
        op.classList.add("pure-u-1-2");
        op.innerText = comment["user"]["full_name"];

        let dates = document.createElement("div");
        dates.classList.add("pure-u-1-2");
        dates.classList.add("text-right");
        dates.innerText = strftime(TIME_FORMAT, new Date(comment["updated_at"]));

        header.appendChild(op);
        header.appendChild(dates);

        article.appendChild(header);

        let body = document.createElement("div");

        if ( comment["type"] == "comment") {
            parseMarkdown("comment", comment["body"], false, function(body_html) {
                body.innerHTML = body_html;
                article.classList.remove("hidden");
            });
        } else if (comment["type"] == "close") {
            if (issue_type == "maintenance") {
                body.innerHTML = "<p>Maintenance complete</p>";
            } else {
                body.innerHTML = "<p>Issue fixed</p>";
            }
            article.classList.add("up");
            article.classList.remove("hidden");
        } else if (comment["type"] == "reopen") {
            if (issue_type == "maintenance") {
                body.innerHTML = "<p>Maintenance restarted</p>";
            } else {
                body.innerHTML = "<p>Issue reopened</p>";
            }
            article.classList.add("down");
            article.classList.remove("hidden");
        }


        article.appendChild(body);

        extra["root"].appendChild(article);

    });
    extra["root"].setAttribute("gitea-page", extra["page"] + 1);
    extra["root"].setAttribute("statea-loading", "0");
}

function parseMarkdown(mode, text, wiki, callback, fail_callback) {

    const init = {
        "method": "POST",
        "body": JSON.stringify({"Context": `/${REPO_OWNER}/${REPO_NAME}`, "Mode": mode, "Text": text, "Wiki": wiki}),
        "headers": {
            "Content-Type": "application/json",
            "Accept": "text/html"
        },
        "mode": CORS_MODE
    }
    const uri = API_URL + "/markdown";
    const load = addLoadingScreen(undefined, uri);
    fetch(uri, init).then((response) => {
        if (!response.ok) {
            console.error("Unable to parse Markdown");
            if (fail_callback != undefined) {
                fail_callback(undefined);
            }
        }
        return response;
    }).then((response) => response.text()).then((data) => {
        callback(data);
    }).catch((error) => {
        console.error("Unable to parse Markdown:", error);
        if (fail_callback != undefined) {
            fail_callback(error);
        }
    }).finally( () => {
        delLoadingScreen(load);
    });
}
