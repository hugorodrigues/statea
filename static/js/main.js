// SPDX-FileCopyrightText: 2022 Hugo Rodrigues
//
// SPDX-License-Identifier: MIT

"use strict";

window.onload = function () {
    setupEvents();
    // Get repository information
    callGitea(`/repos/${REPO_OWNER}/${REPO_NAME}`, function(data) {
        document.getElementById("gitea-repo").href = data["html_url"];
    });
    window.dispatchEvent(new HashChangeEvent("hashchange"));
}
