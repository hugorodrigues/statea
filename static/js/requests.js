"use strict";

// Since we only consume data, we don't need
// a complicated implementation of a wrapper
function requestGetJson(uri, callback, extra, fail_callback, block) {
    const init = {"method": "GET", "headers": {"Accept": "application/json"}, "mode": CORS_MODE};
    let load = undefined;
    if (block == undefined || block) {
        load = addLoadingScreen(undefined, uri);
    }
    fetch(uri, init).then((response) => {
        if (!response.ok) {
            console.error("Invalid response for request");
            if (fail_callback != undefined) {
                fail_callback(undefined);
            }
        }
        return response;
    }).then((response) => response.json()).then((data) => {
        callback(data, extra);
    }).catch((error) => {
        console.error("Unable to complete request:", error);
        if (fail_callback != undefined) {
            fail_callback(error);
        }
    }).finally( () => {
        if (block == undefined || block) {
            delLoadingScreen(load);
        }
    });
}

function requestGetText(uri, callback, extra, fail_callback, block) {
    const init = {"method": "GET", "mode": CORS_MODE};
    let load = undefined;
    if (block == undefined || block) {
        load = addLoadingScreen(undefined, uri);
    }
    fetch(uri, init).then((response) => {
        if (!response.ok) {
            console.error("Invalid response for request");
            if (fail_callback != undefined) {
                fail_callback(undefined);
            }
        }
        return response;
    }).then((response) => response.text()).then((data) => {
        callback(data, extra);
    }).catch(error => {
        console.error("Unable to complete request:", error);
        if (fail_callback != undefined) {
            fail_callback(error);
        }
    }).finally( () => {
        if (block == undefined || block) {
            delLoadingScreen(load);
        }
    });
}

function callGitea(path, callback, extra, fail_callback) {
    const uri = API_URL + path;
    requestGetJson(uri, callback, extra, fail_callback);
}

